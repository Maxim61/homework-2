//
//  SPIStarObject.m
//  Lection2
//
//  Created by Admin on 04.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPIStar.h"

@implementation SPIStar

- (instancetype)initWithName:(NSString *)name
{
    self = [super initWithType:SPISpaceObjectTypeStar name:name];
    self.isAlive = YES;
    return self;
}

- (void) nextTurn
{
    [super nextTurn];
    if (self.mass != 0)
    {
        self.mass -= (unsigned int)arc4random() % 10000;
    }
    else
    {
        self.isAlive = NO;
    }
}

- (NSString *) description
{
    return [NSString stringWithFormat: @"\nStar: %@, \nAlive: %@, \nMass: %ld", self.name, self.isAlive ? @"YES" : @"NO", self.mass];
}
@end
