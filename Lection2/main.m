//
//  main.m
//  Lection2
//
//  Created by Vladislav Grigoriev on 04/10/16.
//  Copyright © 2016 Inostudio. All rights reserved.
//
#import <stdio.h>
#import <Foundation/Foundation.h>
#import "SPIStarSystem.h"
#import "SPISpaceObject.h"
#import "SPIPlayerSpaceship.h"

int main(int argc, const char * argv[]) {

    SPIStarSystem *alphaStarSystem = [[SPIStarSystem alloc] initWithName:@"Sirius" age:@(230000000)];
    
    SPIStar *alphaStar = [[SPIStar alloc] initWithName:@"Alpha"];
    alphaStar.mass = 123496464248;
    
    SPIPlanet *vulkanPlanet = [[SPIPlanet alloc] initWithName:@"Vulcan"];
    vulkanPlanet.atmosphere = YES;
    vulkanPlanet.peoplesCount = 528102000;

    SPIAsteroidField *hotaAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Hota"];
    hotaAsteroidField.density = 6000000;
    
    SPITeleport *AlphaTeleporter = [[SPITeleport alloc] initWithName:@"Alpha Teleporter"];
    
    SPIPlanet *gallifreiPlanet = [[SPIPlanet alloc] initWithName:@"Gallifrey"];
    gallifreiPlanet.atmosphere = YES;
    gallifreiPlanet.peoplesCount = 700000000;
    
    SPIPlanet *nabooPlanet = [[SPIPlanet alloc] initWithName:@"Naboo"];
    nabooPlanet.atmosphere = YES;
    nabooPlanet.peoplesCount = 625000000;
    
    SPIPlanet *plutoPlanet = [[SPIPlanet alloc] initWithName:@"Pluto"];
    plutoPlanet.atmosphere = NO;
    
    SPIStarSystem *SentaSystem = [[SPIStarSystem alloc] initWithName:@"Senta" age:@(7223220000)];
    
    SPIStar *GuardianStar = [[SPIStar alloc] initWithName:@"Guardian"];
    GuardianStar.mass = 6336777323400;
    
    SPITeleport *GuardianTeleporter = [[SPITeleport alloc] initWithName:@"Guardian Teleport"];
    
    SPIPlanet *DendiPlanet = [[SPIPlanet alloc] initWithName:@"Dendi"];
    DendiPlanet.atmosphere = NO;
    DendiPlanet.peoplesCount = 0;
    
    SPIPlanet *SiezedPlanet = [[SPIPlanet alloc] initWithName:@"Siezed"];
    SiezedPlanet.atmosphere = YES;
    SiezedPlanet.peoplesCount = 1231301231;
    
    SPIAsteroidField *LestraAsteroidField = [[SPIAsteroidField alloc] initWithName:@"Lestra"];
    LestraAsteroidField.density = 23131112;

    SPIPlayerSpaceship *spaceship = [[SPIPlayerSpaceship alloc] initWithName:@"Lamber"];
    
    SentaSystem.spaceObjects = [NSMutableArray arrayWithCapacity:6];
    [SentaSystem.spaceObjects addObject:GuardianTeleporter];
    [SentaSystem.spaceObjects addObject:GuardianStar];
    [SentaSystem.spaceObjects addObject:DendiPlanet];
    [SentaSystem.spaceObjects addObject:LestraAsteroidField];
    [SentaSystem.spaceObjects addObject:SiezedPlanet];
    
    alphaStarSystem.spaceObjects = [NSMutableArray arrayWithCapacity:7];
    [alphaStarSystem.spaceObjects addObject:AlphaTeleporter];
    [alphaStarSystem.spaceObjects addObject:vulkanPlanet];
    [alphaStarSystem.spaceObjects addObject:hotaAsteroidField];
    [alphaStarSystem.spaceObjects addObject:gallifreiPlanet];
    [alphaStarSystem.spaceObjects addObject:nabooPlanet];
    [alphaStarSystem.spaceObjects addObject:plutoPlanet];
    [alphaStarSystem.spaceObjects addObject:alphaStar];
    
    NSMutableArray *gameObjects = [[NSMutableArray alloc] init];
    [gameObjects addObjectsFromArray:alphaStarSystem.spaceObjects];
    [gameObjects addObjectsFromArray:SentaSystem.spaceObjects];
    [gameObjects addObject:spaceship];
    
    NSMutableArray *StarSystemsArray = [[NSMutableArray alloc] init];
    [StarSystemsArray addObject:alphaStarSystem];
    [StarSystemsArray addObject:SentaSystem];
    spaceship.allStarSystems = StarSystemsArray;
    spaceship.currentStarSystem = StarSystemsArray[0];
    spaceship.currentSpaceObject = AlphaTeleporter;
    
    NSMutableArray *Teleports = [[NSMutableArray alloc] init];
    [Teleports addObject:AlphaTeleporter];
    [Teleports addObject:GuardianTeleporter];
    AlphaTeleporter.TeleportsArray = Teleports;
    GuardianTeleporter.TeleportsArray = Teleports;
    
    BOOL play = YES;
    
    while (play) {
        SPIPlayerSpaceshipResponse response = [spaceship waitForCommand];
        
        if (response == SPIPlayerSpaceshipResponseExit) {
            play = NO;
            continue;
        }
        
        for (id<SPIGameObject> gameObject in gameObjects) {
            [gameObject nextTurn];
        }
    }
    return 0;
}


