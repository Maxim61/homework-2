//
//  SPIStarObject.h
//  Lection2
//
//  Created by Admin on 04.11.16.
//  Copyright © 2016 Inostudio. All rights reserved.
//

#import "SPISpaceObject.h"

@interface SPIStar : SPISpaceObject

@property (nonatomic, assign) NSInteger mass;
@property (nonatomic, assign) BOOL isAlive;

- (instancetype)initWithName:(NSString *)name;

@end
